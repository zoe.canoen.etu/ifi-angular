# Ifi

## Installation d'Angular

Angular fait parti de l'environnement NodeJs et s'installe avec la commande :

    npm install -g @angular/cli



## Génération initiale du projet

**A NE PAS FAIRE POUR LE TP**

Ce projet a été généré par les commandes :

    npm install -g @angular/cli
    ng new my-app
    cd my-app
    ng serve --open

Si vous souhaitez travailler chez vous, vous pourrez refaire ces commandes. Ici le projet est déjà généré.

## Serveur de développement

Pour lancer le serveur, faites `npm start`. Aller sur `http://localhost:4200/`. L'application va se recharger automatiquement à chaque sauvegarde de fichier.

## Génération automatique de code

Lancez `ng generate component component-name` pour générer un nouveau composant.
Vous pouvez aussi utiliser `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Questions

Le but du tp est de créer un site de news.
Pour cela vous aurez à votre disposition des données contenues dans le fichier `src/app/mocks/articles.ts`.

#### Accueil
![GitHub Logo](images/Accueil.PNG)

#### Résumé
![GitHub Logo](images/Abstract.PNG)

#### Détail
![GitHub Logo](images/Detail.PNG)

#### Filtre
![GitHub Logo](images/Filter.PNG)


Angular est un langage front, c'est pourquoi nous allons simuler un back.
Si vous cherchez dans le code, vous trouverez parfois du code qui vous expliquera comment faire si nous avions un back. (ref : src/app/shared/services/articles.service.ts).

### L'accueil

Pour l'accueil, nous allons d'aboard afficher la liste des articles dans le fichier `src\app\components\articles-list\articles-list.component.ts`

Pour cela il faut importer la liste des articles et sauvegarder les articles dans le composant.
Une liste d'article est mise à votre disposition dans `src/app/shared/mocks/articles`.

#### REPONSE

Remplacer ce code dans les fichiers 
*src\app\components\articles-list\articles-list.component.ts*
```ts
import { articles } from 'src/app/shared/mocks/articles';

@Component({
  selector: 'app-articles-list',
  templateUrl: './articles-list.component.html',
  styleUrls: ['./articles-list.component.css']
})

export class ArticlesListComponent implements OnInit {
public articles;

// Les constructeurs de composants sont toujours vide
constructor() { }

// Création du composant
ngOnInit() {
  // On sauvegarde la resource du mock dans le composant
  this.articles = articles;
}
}
```

*src\app\components\articles-list\articles-list.component.html*
```html
<div class="row ">
    <div  class="col-12 mat-card article" *ngFor="let article of articles; let i = index">
      {{article.name}}
    </div>
</div>
```

### Les Classes

Pour être sûr que l'on récupère bien une liste d'Article nous allons créer une classe dans `src/app/shared/models/article.model.ts`,
et on type l'attribut du composant articles-list.

#### REPONSE

*src/app/shared/models/article.model.ts*
```ts
export class Article {
  name: string;
  year: number;
  abstract: string;
  url: string;
}
```

On type l'attribut

*src\app\components\articles-list\articles-list.component.ts*
```ts
public articles: Article[];
```

Et on n'oublie pas d'importer la ressource.

*src\app\components\articles-list\articles-list.component.ts*
```ts
import { Article } from 'src/app/shared/models/article.model';
```

### Création d'un composant

Création d'un composant. Faites `ng generate component components/my-component`. 
Cela va créer un dossier `components/my-component` contenant un fichier html, css, ts et spec.ts (pour les tests).

#### Le header

Nous appelerons ce composant `header`.

A l'aide de material.angular nous allons créer une Toolbar.
Cette Toolbar servira pour le header.

https://material.angular.io/components/toolbar/overview

Le code à ajouter est ici :

![GitHub Logo](images/material-tuto-1.PNG)

Les imports à faire se font ici :

![GitHub Logo](images/material-tuto-2.PNG)


Nous allon également créer un filter que nous configurerons à la fin.

https://material.angular.io/components/input/overview

#### L'abstract déroulant

Nous appelerons ce composant `articles-list-abstract`.

Cela sera le résumé de l'article. Nous voulons que ce résumé soit une liste déroulante.

https://material.angular.io/components/expansion/overview

#### REPONSE

*src\app\components\articles-list-abstract\articles-list-abstract.component.html*
```html
<mat-expansion-panel>
    <mat-expansion-panel-header>
      <mat-panel-description>
          Abstract
      </mat-panel-description>
    </mat-expansion-panel-header>
    {{article.abstract}}
</mat-expansion-panel>
```

Ajout de material pour intégrer directement un joli css
```
ng add @angular/material
```

Ajout d'un composant dans un composant avec passage de paramètre

*src\app\components\articles-list\articles-list.component.html*
```html
<div class="row ">
    <div  class="col-12 mat-card article" *ngFor="let article of articles; let i = index">
      {{article.name}}
      <app-articles-list-abstract [article]="article"></app-articles-list-abstract>
    </div>
</div>
```

### Utilisation d'un service

La première question n'étais pas très satisfaisant d'un point de vue webService. En effet, si nous avions un vrai projet, il nous faudrait
créer et utiliser des webServices. Les webServices sont exposés grâce au back (que nous n'avons pas), nous allons donc le simuler. 

Utilisons donc la classe getArticles qui renvoie un Observables d'article.
Un observable permet de faire appel à des API synchrones ou asynchrones. Il suffit donc de s'abonner à la resource proposée par le service, et se désabonner à la fin.

```
npm install rxjs-compat --save
```

#### REPONSE

*src\app\components\articles-list\articles-list.component.ts*
```ts

// Import du composant, de OnInit et OnDestroy
import { Component, OnInit, OnDestroy } from '@angular/core';
// Ajout des classes
import { Article } from 'src/app/shared/models/article.model';
// Ajout des services
import { ArticlesService } from 'src/app/shared/services/articles.service';
// Ajout des abonnement
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-articles-list',
  templateUrl: './articles-list.component.html',
  styleUrls: ['./articles-list.component.css']
})

// Cette méthode implémente OnInit ET OnDestroy
export class ArticlesListComponent implements OnInit, OnDestroy {

public articles: Article[]
private articlesSubscription: Subscription;

constructor(private articlesService: ArticlesService) { }

// Création du composant
ngOnInit() {
  // On sauvegarde l'abonnement sur la resource
  this.articlesSubscription = this.articlesService.getArticles().subscribe(
    // Cast
    (articlesList: Article[]) => {
      // On sauvegarde la liste des articles du service dans le composant
      this.articles = articlesList;
    }
  );
}

// Destruction du composant
ngOnDestroy() {
  // On retire l'abonnement
  this.articlesSubscription.unsubscribe();
}
```

### Utilisation d'un routeur

Création d'un routeur. Nous allons lier un composant à un autre. 
Nous allons créer un composant article-detail.
Si l'on clique sur le titre de l'article dans l'accueil, cela doit nous rediriger vers une page qui contient le nom de l'article, l'année, le résumé et l'année.

#### REPONSE

*articles-list.component.html*
```html
<a [title]="article.name + ' details'" [routerLink]="['/articles', articleId]">
  {{ article.name }}
</a>
```

*app.module.ts*
```ts
@NgModule({
  imports: [
    ...
    RouterModule.forRoot([
    { path: '', component: ArticlesListComponent },
    { path: 'articles/:articleId', component: ArticleDetailComponent }
  ])
  ...
```

*article-detail.component.html*
```html
<h2>Article Details</h2>

<div *ngIf="article">
  <h3>{{ article.name }}</h3>
  <h4>{{ article.year }}</h4>
  <p>{{ article.abstract }}</p>
  <a href={{article.url}}>{{ article.url }}</a>
</div>
```

*article-detail.component.ts*
```ts
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { articles } from 'src/app/shared/mocks/articles';
import { Article } from 'src/app/shared/models/article.model';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.css']
})
export class ArticleDetailComponent implements OnInit {
  public article: Article;

  constructor(
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.article = articles[+params.get('articleId')];
    });
  }
}
```
### Création d'un filter

Pour cela il faut changer le service d'affichage des articles. En effet, le filter va s'abonner au changement de l'input 

#### REPONSE

```ts
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ArticlesService } from 'src/app/shared/services/articles.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  public form: FormGroup;
  private subscription: Subscription;
  constructor(
    private formBuilder: FormBuilder, private articlesService: ArticlesService
  ) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      search: ''
    });
    this.subscription = this.form.get('search').valueChanges.subscribe(val => {
      this.articlesService.filterArticles(val);
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
```

*src/app/shared/services/articles.service.ts*
```ts
import { Injectable } from '@angular/core';
import { Article } from 'src/app/shared/models/article.model';
import { of, Observable, Subject, ReplaySubject } from 'rxjs';
import { articles } from 'src/app/shared/mocks/articles';

/** ArticlesService qui renvoie la liste des articles */
@Injectable({
  providedIn: 'root'
})
export class ArticlesService {
  private articles: Article[] = articles;
  private displayedArticles: Article[] = articles;
  private articlesSubject: Subject<Article[]> = new ReplaySubject<Article[]>(1);

  constructor() {
    this.articlesSubject.next(this.displayedArticles);
  }

  getArticles(): Observable<Article[]> {
    return this.articlesSubject.asObservable();
  }

  filterArticles(val: string) {
    if (val === '') {
      this.displayedArticles = this.articles;
    } else {
      this.displayedArticles = this.articles.filter((article: Article) => article.name.toLowerCase().includes(val.toLowerCase()));
    }
    this.articlesSubject.next(this.displayedArticles);
  }

  /* Exemple si jamais nous avions un back
    import { Http, Response } from '@angular/http';

    constructor(private http: Http) { }

    getArticles() {
      return this.http.get('/api/articles')
        .map((res: Response) => res.json().response);
    }
    */
}
```
