import { Component, OnInit, Input } from '@angular/core';
import { Article } from 'src/app/shared/models/article.model';

@Component({
  selector: 'app-articles-list-abstract',
  templateUrl: './articles-list-abstract.component.html',
  styleUrls: ['./articles-list-abstract.component.css']
})
export class ArticlesListAbstractComponent implements OnInit {

  @Input() article: Article;

  constructor() { }

  ngOnInit() {
  }

}
