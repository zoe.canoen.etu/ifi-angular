import { Component, OnInit, OnDestroy } from '@angular/core';
import { Article } from 'src/app/shared/models/article.model';
import { ArticlesService } from 'src/app/shared/services/articles.service';
import { articles } from 'src/app/shared/mocks/articles';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-articles-list',
  templateUrl: './articles-list.component.html',
  styleUrls: ['./articles-list.component.css']
})

export class ArticlesListComponent implements OnInit, OnDestroy {

  // A supprimer

  public articles: Article[];

  // ########## Méthode sans les services ##########
  /*
    constructor() { }

    // Création du composant
    ngOnInit() {
      // On sauvegarde la resource du mock dans le composant
      this.articles = articles;
    }
  */

  // ########## Méthode avec des services ##########
  private articlesSubscription: Subscription;

  constructor(private articlesService: ArticlesService) { }

  // Création du composant
  ngOnInit() {
    // On sauvegarde l'abonnement sur la resource
    this.articlesSubscription = this.articlesService.getArticles().subscribe(
      // Cast
      (articlesList: Article[]) => {
        // On sauvegarde la liste des articles du service dans le composant
        this.articles = articlesList;
      }
    );
  }

  // Destruction du composant
  ngOnDestroy() {
    // On retire l'abonnement
    this.articlesSubscription.unsubscribe();
  }

}
