import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ArticlesService } from 'src/app/shared/services/articles.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  public form: FormGroup;
  private subscription: Subscription;
  constructor(
    private formBuilder: FormBuilder, private articlesService: ArticlesService
  ) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      search: ''
    });
    this.subscription = this.form.get('search').valueChanges.subscribe(val => {
      this.articlesService.filterArticles(val);
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}

