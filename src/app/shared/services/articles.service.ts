import { Injectable } from '@angular/core';
import { Article } from 'src/app/shared/models/article.model';
import { of, Observable, Subject, ReplaySubject } from 'rxjs';
import { articles } from 'src/app/shared/mocks/articles';

/** ArticlesService qui renvoie la liste des articles */
@Injectable({
  providedIn: 'root'
})
export class ArticlesService {
  private articles: Article[] = articles;
  private displayedArticles: Article[] = articles;
  private articlesSubject: Subject<Article[]> = new ReplaySubject<Article[]>(1);

  constructor() {
    this.articlesSubject.next(this.displayedArticles);
  }

  getArticles(): Observable<Article[]> {
    return this.articlesSubject.asObservable();
  }

  filterArticles(val: string) {
    if (val === '') {
      this.displayedArticles = this.articles;
    } else {
      this.displayedArticles = this.articles.filter((article: Article) => article.name.toLowerCase().includes(val.toLowerCase()));
    }
    this.articlesSubject.next(this.displayedArticles);
  }

  /* Exemple si jamais nous avions un back
    import { Http, Response } from '@angular/http';

    constructor(private http: Http) { }

    getArticles() {
      return this.http.get('/api/articles')
        .map((res: Response) => res.json().response);
    }
    */
}
