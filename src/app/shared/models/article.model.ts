export class Article {
  name: string;
  year: number;
  abstract: string;
  url: string;
}
